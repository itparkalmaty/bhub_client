import {serverApi} from "./user";
import axios from "axios";
import {
    CHOOSE_BUSINESS, CITIES, CURRENCY,
    GET_BUSINESS,
    GET_BUSINESSES,
    GET_CATEGORIES,
    GET_TYPES,
    HIDE_LOADER,
    SHOW_LOADER
} from "../types";
import Swal from "sweetalert2";
import {useSelector} from "react-redux";

export function fetchBusinesses(type) {
    return async dispatch => {
        axios.get(`${serverApi}/businesses/${type}`)
            .then(response => dispatch({type: GET_BUSINESSES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchTypes(id=null) {
    return async dispatch => {
        axios.get(`${serverApi}/businesses/types/${id != null ? id : ''}`)
            .then(response => dispatch({type: GET_TYPES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchCategories(id) {
    return async dispatch => {
        axios.get(`${serverApi}/businesses/categories/${id}`)
            .then(response => dispatch({type: GET_CATEGORIES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchByBusinessCategory(url) {
    return async dispatch => {
        axios.get(url)
            .then(response => dispatch({type: GET_BUSINESSES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchByName(name, id) {
    return async dispatch => {
        axios.get(`${serverApi}/businesses/${id}?search=${name}`)
            .then(response => dispatch({type: GET_BUSINESSES, payload: response.data.data}))
            .catch(e => console.log(e))

    }
}

export function fetchBusiness(slug, user) {
    return async dispatch => {
        dispatch({type: SHOW_LOADER})
        const token = localStorage.getItem('token')
        axios.get(`${serverApi}/businesses/show/${slug}${user ? `?user_id=${user}` : ''}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
            .then(response => {
                dispatch({type: CHOOSE_BUSINESS, payload: response.data.data})
                setTimeout(() => dispatch({type: HIDE_LOADER})
                ,1000)
            })
            .catch(e => console.log(e))
    }
}

export function chooseBusiness(business) {
    return async dispatch => {
        dispatch({type: CHOOSE_BUSINESS, payload: business})
    }
}

export function storeBusiness(form, type) {
    return async dispatch => {
        const token = localStorage.getItem('token')
        var bodyFormData = new FormData()
        for (let key in form) {
            if (key !== 'images') {
                bodyFormData.append(key, form[key])
            }
        }
        if (form.images.length) {
            form.images.forEach(image => {
                bodyFormData.append('images[]', image)
            })
        }
        axios.post(`${serverApi}/businesses`, bodyFormData, {
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "multipart/form-data"
            },

        }).then(response => {
            Swal.fire({
                title: 'Успешно',
                text: response.data.message,
                icon: 'success',
                confirmButtonText: 'Хорошо'
            })
            dispatch(fetchBusinesses(type))
        }).catch(e => {
            let errors = []

            for (let error in e.response.data.errors) {
                errors.push(e.response.data.errors[error][0])
            }
            Swal.fire({
                title: 'Ошибка',
                text: errors.join('\n'),
                icon: 'error',
                confirmButtonText: 'Ок'
            })
        })
    }
}

export function fetchCities() {
    return async dispatch => {
        axios.get(`${serverApi}/businesses/cities`)
            .then(response => dispatch({type: CITIES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchCurrencies() {
    return async dispatch => {
        axios.get(`${serverApi}/businesses/currencies`)
            .then(response => dispatch({type: CURRENCY, payload: response.data.currency}))
            .catch(e => console.log(e))
    }
}

export function fetchAllCategories(id=null) {
    return async dispatch => {
        axios.get(`${serverApi}/businesses/categories/all${id != null ? `?type=${id}` : ''}`)
            .then(response => dispatch({type: GET_CATEGORIES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}
