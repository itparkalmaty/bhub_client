import {serverApi} from "./user";
import {FAQ, INVESTORS, SETTINGS} from "../types";
import axios from "axios";

export function fetchSettings() {
    return async dispatch=>{
        axios.get(`${serverApi}/settings`)
            .then(response=>dispatch({type:SETTINGS,payload:response.data.settings}))
            .catch(e=>console.log(e))
    }
}

export function fetchFAQ() {
    return async dispatch=>{
        axios.get(`${serverApi}/faq`)
            .then(response=>dispatch({type:FAQ,payload:response.data.data}))
            .catch(e=>console.log(e))
    }
}

export function fetchInvestors() {
    return async dispatch=>{
        axios.get(`${serverApi}/investors`)
            .then(response=>dispatch({type:INVESTORS,payload:response.data.data}))
            .catch(e=>console.log(e))
    }
}