import axios from "axios";
import Swal from "sweetalert2";
import {LOGIN, LOGOUT, USER_BUSINESSES, USER_VIEWS} from "../types";
import {useRouter} from "next/router";
import {fetchFreelancers} from "./freelancer";

export const serverApi = 'https://api.bhub.kz/api'

export function login(form) {

    return async dispatch => {
        axios.post(`${serverApi}/auth/login`, form).then(response => {
            dispatch({type: LOGIN, payload: response.data.user})
            localStorage.setItem('token', response.data.token)
            Swal.fire({
                title: 'Успешно',
                text: response.data.message,
                icon: 'success',
                confirmButtonText: 'Хорошо'
            })

        }).catch(e => {
            let errors = []

            for (let error in e.response.data.errors) {
                errors.push(e.response.data.errors[error][0])
            }
            Swal.fire({
                title: 'Ошибка',
                text: errors.join('\n'),
                icon: 'error',
                confirmButtonText: 'Хорошо'
            })
        })
    }
}

export function logout() {
    return async dispatch => {
        localStorage.removeItem('token')
        Swal.fire({
            title: 'Успешно',
            text: 'Вы успешно вышли',
            icon: 'success',
            confirmButtonText: 'Хорошо'
        })
        dispatch({type: LOGOUT})

    }
}

export function register(form) {
    return async dispatch => {
        axios.post(`${serverApi}/auth/register`, form).then(response => {
            dispatch({type: LOGIN, payload: response.data.user})
            localStorage.setItem('token', response.data.token)
            Swal.fire({
                title: 'Успешно',
                text: response.data.message,
                icon: 'success',
                confirmButtonText: 'Хорошо'
            })
        }).catch(e => {
            let errors = []

            for (let error in e.response.data.errors) {
                errors.push(e.response.data.errors[error][0])
            }
            Swal.fire({
                title: 'Ошибка',
                text: errors.join('\n'),
                icon: 'error',
                confirmButtonText: 'Хорошо'
            })
        })
    }
}

export function fetchUser() {
    return async dispatch => {
        const token = localStorage.getItem('token')
        axios.get(`${serverApi}/user`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        }).then(response => dispatch({type: LOGIN, payload: response.data.user}))
            .catch(e => console.log(e))
    }
}

export function fetchViews() {
    return async dispatch => {
        const token = localStorage.getItem('token')
        axios.get(`${serverApi}/user/views`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        }).then(response => dispatch({type: USER_VIEWS, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function fetchUserBusinesses(id) {
    return async dispatch => {
        const token = localStorage.getItem('token')
        axios.get(`${serverApi}/user/businesses?business_type_id=${id}`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
            .then(response => dispatch({type: USER_BUSINESSES, payload: response.data.data}))
            .catch(e => console.log(e))
    }
}

export function updateUser(form) {
    return async dispatch => {
        const token = localStorage.getItem('token')
        axios.post(`${serverApi}/user`, form, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        }).then(response => {
            Swal.fire({
                title: 'Успешно',
                text: 'Ваш аккаунт успешно обновлен',
                icon: 'success',
                confirmButtonText: 'Хорошо'
            })
            dispatch(fetchUser())
        }).catch(e => console.log(e))
    }
}


export function updateFreelancer(form) {
    return async dispatch => {
        let bodyFormData = new FormData()
        for (let key in form) {
            bodyFormData.append(key, form[key])
        }
        const token = localStorage.getItem('token')
        axios.post(`${serverApi}/user/freelancer`, bodyFormData, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        }).then(response => {
            Swal.fire({
                title: 'Успешно',
                text: 'Ваш аккаунт успешно обновлен',
                icon: 'success',
                confirmButtonText: 'Хорошо'
            })
            dispatch(fetchUser())
        }).catch(e => {
            let errors = []

            for (let error in e.response.data.errors) {
                errors.push(e.response.data.errors[error][0])
            }
            Swal.fire({
                title: 'Ошибка',
                text: errors.join('\n'),
                icon: 'error',
                confirmButtonText: 'Хорошо'
            })
        })
    }
}


export function storeFreelancerAccount(form) {
    return async dispatch => {
        let bodyFormData = new FormData()
        for (let key in form) {
            bodyFormData.append(key, form[key])
        }
        const token = localStorage.getItem('token')
        axios.post(`${serverApi}/freelancers`, bodyFormData, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        }).then(response => {

            Swal.fire({
                title: 'Успешно',
                text: 'Ваш аккаунт успешно создан',
                icon: 'success',
                confirmButtonText: 'Хорошо'
            })
            dispatch(fetchUser())
            dispatch(fetchFreelancers())
        }).catch(e => {
            let errors = []

            for(let error in e.response.data.errors){
                errors.push(e.response.data.errors[error][0])
            }
            Swal.fire({
                title: 'Ошибка',
                text: errors.join('\n'),
                icon: 'error',
                confirmButtonText: 'Хорошо'
            })
        })
    }
}