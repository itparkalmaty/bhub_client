import axios from "axios";
import {serverApi} from "./user";
import {GET_ARTICLE, GET_ARTICLE_CATEGORIES, GET_ARTICLES} from "../types";

export function fetchArticles() {
    return async dispatch=>{
        axios.get(`${serverApi}/article`).then(response=>{
            dispatch({type:GET_ARTICLES,payload:response.data.data})
        }).catch(e=>console.log(e))
    }
}

export function fetchArticle(slug) {
    return async dispatch=>{
        axios.get(`${serverApi}/article/${slug}`)
            .then(response=>dispatch({type:GET_ARTICLE,payload:response.data.data}))
            .catch(e=>console.log(e))
    }
}

export function fetchArticleCategories() {
    return async dispatch=>{
        axios.get(`${serverApi}/article/categories`)
            .then(response=>dispatch({type:GET_ARTICLE_CATEGORIES,payload:response.data.data}))
            .catch(e=>console.log(e))
    }
}

export function fetchArticleByCategoryId(url) {
    return async dispatch=>{
        axios.get(url)
            .then(response=>dispatch({type:GET_ARTICLES,payload:response.data.data}))
            .catch(e=>console.log(e))
    }
}
export function searchArticle(word) {
    return async dispatch=>{
        axios.get(`${serverApi}/article?search=${word}`)
            .then(response=>dispatch({type:GET_ARTICLES,payload:response.data.data}))
            .catch(e=>console.log(e))
    }
}