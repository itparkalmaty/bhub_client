import axios from "axios";
import {logout, serverApi} from "./user";
import {GET_CATEGORY_FREELANCERS, GET_FREELANCER, GET_FREELANCERS} from "../types";
import {hideLoading, showLoading} from "sweetalert2";

export function fetchFreelancers() {
    return async dispatch=>{
        axios.get(`${serverApi}/freelancers`).then(response=>{
            dispatch({type:GET_FREELANCERS,payload: response.data.freelancers})
        }).catch((e)=>{
            console.log(e.response)
        })
    }

}

export function fetchCategories() {
    return async dispatch=>{
        axios.get(`${serverApi}/freelancers/categories`).then(response=>{
            dispatch({type:GET_CATEGORY_FREELANCERS,payload:response.data.data})
        }).catch(e=>console.log(e))
    }
}

export function fetchByCategory(link){
    return async dispatch=>{
        axios.get(link).then(response=>{
            dispatch({type:GET_FREELANCERS,payload: response.data.freelancers})
        }).catch((e)=>{
            console.log(e.response)
        })
    }
}

export function searchFreelancers(name) {
    return async dispatch=>{
        axios.get(`${serverApi}/freelancers?search=${name}`)
            .then(response=>dispatch({type:GET_FREELANCERS,payload:response.data.freelancers}))
            .catch(e=>console.log(e))
    }
}

export function fetchFreelancer(slug) {
    return async dispatch=>{
        axios.get(`${serverApi}/freelancers/${slug}`)
            .then(response=> {
                dispatch({type: GET_FREELANCER, payload: response.data.data})
            })
            .catch(e=>console.log(e))
    }
}