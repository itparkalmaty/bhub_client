import {createStore, applyMiddleware, compose, combineReducers} from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension/index";
import {rootReducer} from "./reducers/rootReducer";
const mainReducer = combineReducers({
    root: rootReducer
})

const store = createStore(mainReducer,compose(
    applyMiddleware(
        thunk
    ),
))
export default store