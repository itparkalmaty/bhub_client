import {BUSINESS_TYPE, CHOOSE_BUSINESS, CITIES, CURRENCY, GET_BUSINESSES, GET_CATEGORIES, GET_TYPES} from "../types";

const initialState = {
    businesses: [],
    types:[],
    categories:[],
    mainBusiness:{},
    currencies:[],
    cities:[],
}

export const businessReducer = (state=initialState,action)=>{
    switch (action.type){
        case GET_BUSINESSES:
            return {...state,businesses: action.payload}
        case GET_TYPES:
            return {...state,types: action.payload}
        case GET_CATEGORIES:
            return {...state,categories: action.payload}
        case CHOOSE_BUSINESS:
            return {...state,mainBusiness: action.payload}
        case CITIES:
            return {...state,cities: action.payload}
        case CURRENCY:
            return {...state,currencies: action.payload}
        case BUSINESS_TYPE:
            return {...state,types: action.payload}
        default:
            return  state
    }
}