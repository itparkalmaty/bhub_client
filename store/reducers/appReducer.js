import {FAQ, HIDE_LOADER, INVESTORS, SETTINGS, SHOW_LOADER} from "../types";

const initialState = {
    loading: false,
    settings:{},
    faq: [],
    investors:[]
}

export const appReducer = (state = initialState, action) => {

    switch (action.type){
        case HIDE_LOADER:
            return {...state,loading: false}
        case SHOW_LOADER:
            return {...state,loading: true}
        case SETTINGS:
            return {...state,settings: action.payload}
        case INVESTORS:
            return {...state,investors: action.payload}
        case FAQ:
            return {...state,faq:action.payload}
        default:
            return  state
    }
}