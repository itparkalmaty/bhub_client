import {LOGIN, LOGOUT, USER_BUSINESSES, USER_VIEWS} from "../types";

const initialState = {
    user: {},
    views:[],
    businesses:[]
}

export const userReducer = (state=initialState,action)=>{
    switch (action.type){
        case LOGIN:
            return {...state,user: action.payload}
        case LOGOUT:
            return {...state,user: {}}
        case USER_VIEWS:
            return {...state,views: action.payload}
        case USER_BUSINESSES:
            return {...state,businesses: action.payload}
        default:
            return  state
    }
}