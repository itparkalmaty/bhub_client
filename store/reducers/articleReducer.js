import {GET_ARTICLE, GET_ARTICLE_CATEGORIES, GET_ARTICLES} from "../types";

const initialState = {
    articles:[],
    article:{},
    categories:[]
}

export const articleReducer = (state = initialState,action) => {
    switch (action.type){
        case GET_ARTICLES:
            return {...state,articles: action.payload}
        case GET_ARTICLE:
            return {...state,article: action.payload}
        case GET_ARTICLE_CATEGORIES:
            return {...state,categories: action.payload}
        default:
            return  state
    }
}