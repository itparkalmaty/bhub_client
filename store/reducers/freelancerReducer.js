import {GET_CATEGORY_FREELANCERS, GET_FREELANCER, GET_FREELANCERS} from "../types";

const initialState = {
    freelancers:[],
    categories: [],
    freelancer:{}
}


export const freelancerReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_FREELANCERS:
            return {...state, freelancers: action.payload}
        case GET_CATEGORY_FREELANCERS:
            return {...state, categories: action.payload}
        case GET_FREELANCER:
            return {...state,freelancer: action.payload}
        default:
            return state
    }
}