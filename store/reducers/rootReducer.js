import {combineReducers} from "redux";
import {userReducer} from "./userReducer";
import {freelancerReducer} from "./freelancerReducer";
import {articleReducer} from "./articleReducer";
import {businessReducer} from "./businessReducer";
import {appReducer} from "./appReducer";

export const rootReducer = combineReducers({
    user:userReducer,
    freelance: freelancerReducer,
    article:articleReducer,
    business:businessReducer,
    app: appReducer
})