import Layout from "../../components/Layout";
import BusinessList from "../../components/list/BusinessList";

export default function CommercialIndex() {
    return (
        <Layout>
            <div className="startup-news-block">
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Комерческие помещения</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt="" />
                            </div>
                            <div className="main-point">Комерческие помещения</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="item-news-block">
                <BusinessList id={3}/>
            </div>

        </Layout>
    )
}