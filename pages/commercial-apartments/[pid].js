import {useRouter} from "next/router";
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect} from "react";
import {fetchBusiness, fetchCategories} from "../../store/actions/business";
import Layout from "../../components/Layout";
import Sidebar from "../../components/Sidebar";
import Banner from "../../components/Banner";
import BusinessComponent from "../../components/show/BusinessComponent";

export default function Apartment() {
    const router = useRouter()
    const {pid} = router.query

    const dispatch = useDispatch()

    const user = useSelector(state=>state.root.user.user)

    useEffect(() => {
        dispatch(fetchBusiness(pid,user.id))

        dispatch(fetchCategories(3))
    }, [pid, router,user])

    const business = useSelector(state => state.root.business.mainBusiness)
    const categories = useSelector(state => state.root.business.categories)
    const loading = useSelector(state => state.root.app.loading)

    if (Object.keys(business).length === 0)
        return <div/>
    return (
        <Layout>
            <div className="busines-news-block startup-single-block"
                 style={{backgroundImage: 'url(/image/startup.png)'}}>
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Карточка комерческого помещения</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt=""/>
                            </div>
                            <div className="main-point">Карточка комерческого помещения</div>
                        </div>

                    </div>
                </div>
            </div>


            <BusinessComponent categories={categories} business={business}/>
        </Layout>
    )
}