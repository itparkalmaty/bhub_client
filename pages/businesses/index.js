import Layout from "../../components/Layout";
import BusinessList from "../../components/list/BusinessList";

export default function BusinessIndex() {
    return (
        <Layout>
            <div className="startup-news-block">
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Бизнесы</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt="" />
                            </div>
                            <div className="main-point">Бизнесы</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="item-news-block">
                <BusinessList id={2}/>
            </div>

        </Layout>
    )
}