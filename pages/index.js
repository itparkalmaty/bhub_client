import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from "../components/Layout";
import React, {useEffect} from "react";
import Link from "next/link";
import QuestionItem from "../components/items/question";
import {useDispatch, useSelector} from "react-redux";
import {fetchArticles} from "../store/actions/article";
import {fetchBusinesses} from "../store/actions/business";
import {fetchFAQ, fetchInvestors, fetchSettings} from "../store/actions/app";
import InvestorItem from "../components/items/investor";

export default function Home() {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchArticles())
        dispatch(fetchBusinesses(1))
        dispatch(fetchSettings())
        dispatch(fetchFAQ())
        dispatch(fetchInvestors())
    }, [])
    const articles = useSelector(state => state.root.article.articles)
    const businesses = useSelector(state => state.root.business.businesses)
    const settings = useSelector(state => state.root.app.settings)
    const investors = useSelector(state => state.root.app.investors)
    const questions = useSelector(state => state.root.app.faq)
    console.log(settings)
    return (
        <Layout>
            <Head>
                <title>BHUB</title>
            </Head>
            <div className="wrapper-main-page">
                <div className="main-preview">
                    <div className="container">
                        <div className="text-preview">
                            <div className="title-frilans">{settings.banner_title}</div>
                            <div className="item-frilans">
                                <ul>
                                    <li>Стартапы</li>
                                    <li>Действующие бизнесы</li>
                                    <li>Фрилансеры</li>
                                </ul>
                            </div>
                            <div className="btn-group-frilans">
                                <Link href={'/startups'}>
                                    <button className="investor">Найти стартап</button>
                                </Link>
                                <Link href={'/freelancers'}>
                                    <button className="employer">Найти фрилансера</button>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="scroll">
                        <a href="/">
                            <img src="/image/icon/Scroll.png" alt=""/>
                        </a>
                    </div>
                </div>
                <div className="investments">
                    <div className="container">
                        <div className="wrapper-investments">
                            <div className="investments-pucturs">
                                <div className="first-investments-pict">
                                    <img src="/image/pictur/person-1.png" alt=""/>
                                </div>
                                <div className="second-investments-pict">
                                    <img src="/image/pictur/person-2.png" alt=""/>
                                </div>
                            </div>
                            <div className="investments-text">
                                <div className="investments-column">
                                    <div className="prev-icon">
                                        <img src="/image/icon/connect.png" alt=""/>
                                    </div>
                                    <div className="prev-text-column">
                                        <div className="title-column-investments">Инвестируйте</div>
                                        <div className="descripton-column-investments">
                                            {settings.investment_text}
                                        </div>
                                    </div>
                                </div>
                                <div className="investments-column">
                                    <div className="prev-icon">
                                        <img src="image/icon/statistic.png" alt=""/>
                                    </div>
                                    <div className="prev-text-column">
                                        <div className="title-column-investments">Привлекайте</div>
                                        <div className="descripton-column-investments">
                                            {settings.find_investment_text}
                                        </div>
                                    </div>
                                </div>
                                <div className="investments-column">
                                    <div className="prev-icon">
                                        <img src="/image/icon/money.png" alt=""/>
                                    </div>
                                    <div className="prev-text-column">
                                        <div className="title-column-investments">Продавайте</div>
                                        <div className="descripton-column-investments">
                                            {settings.for_investment_text}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="rating-news">
                    <div className="container">
                        <div className="wrapper-column-news">
                            <div className="column-news">
                                <div className="icon-column-news">
                                    <img src="/image/icon/rocket.png" alt=""/>
                                </div>
                                <div className="number-column-news">80+</div>
                                <div className="title-column-news">Открытых бизнесов</div>
                                <div className="subtitle-column-news">
                                    {settings.startups_count_text}
                                </div>
                            </div>
                            <div className="column-news">
                                <div className="icon-column-news">
                                    <img src="/image/icon/linework.png" alt=""/>
                                </div>
                                <div className="number-column-news">10x</div>
                                <div className="title-column-news">Предложений продажи бизнеса</div>
                                <div className="subtitle-column-news">
                                    {settings.businesses_sale_text}
                                </div>
                            </div>
                            <div className="column-news">
                                <div className="icon-column-news">
                                    <img src="/image/icon/like.png" alt=""/>
                                </div>
                                <div className="number-column-news">1.2k</div>
                                <div className="title-column-news">Специалистов</div>
                                <div className="subtitle-column-news">
                                    {settings.investment_count_text}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="investor-startup">
                    <div className="special-container">
                        <div className="wrapper-investor-startup">
                            <div className="wrapper-for-design">
                                <div className="investor-startup-block-desc">
                                    {/*<div className="description-investor-startup">Инвесторы</div>*/}
                                    <div className="title-investor-startup">Инвесторы</div>
                                </div>
                                <div className="investor-startup-block-arrow-slide">
                                    <div className="swiper-button-prev btn-investor-prev">
                                        <img src="/image/icon/wite-arrow.png" alt=""/>
                                    </div>
                                    <div className="swiper-button-next btn-investor-next">
                                        <img src="/image/icon/black-arrow.png" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div className="wrapper-slider-startup">
                                <div className="swiper-container slide-investor-startup ">
                                    <div className="swiper-wrapper">
                                        <div className="swiper-slide slider-str">
                                            {investors.map(investor=><InvestorItem investor={investor}/>)}
                                        </div>
                                        <div className="swiper-slide slider-str">
                                            <div className="startup-column">
                                                <div className="item-startup-prof">
                                                    <div className="item-icon-startup">
                                                        <img src="/image/pictur/investor-1.png" alt=""/>
                                                    </div>
                                                    <div className="item-name-sartup">Иванов Андрей Константинович</div>
                                                </div>
                                                <div className="description-startup-prof">
                                                    По своей сути рыбатекст является альтернативой традиционному lorem
                                                    ipsum,
                                                    который вызывает у
                                                    некторых людей недоумение при попытках прочитать рыбу текст.
                                                </div>
                                                <div className="arrow-wrapper-investor">
                                                    <a href="/"> <img src="/image/icon/right-arrow-blue.png"
                                                                      alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="swiper-slide slider-str">
                                            <div className="startup-column">
                                                <div className="item-startup-prof">
                                                    <div className="item-icon-startup">
                                                        <img src="/image/pictur/investor-1.png" alt=""/>
                                                    </div>
                                                    <div className="item-name-sartup">Иванов Андрей Константинович</div>
                                                </div>
                                                <div className="description-startup-prof">
                                                    По своей сути рыбатекст является альтернативой традиционному lorem
                                                    ipsum,
                                                    который вызывает у
                                                    некторых людей недоумение при попытках прочитать рыбу текст.
                                                </div>
                                                <div className="arrow-wrapper-investor">
                                                    <a href="/"> <img src="/image/icon/right-arrow-blue.png"
                                                                      alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="swiper-slide slider-str">
                                            <div className="startup-column">
                                                <div className="item-startup-prof">
                                                    <div className="item-icon-startup">
                                                        <img src="/image/pictur/investor-1.png" alt=""/>
                                                    </div>
                                                    <div className="item-name-sartup">Иванов Андрей Константинович</div>
                                                </div>
                                                <div className="description-startup-prof">
                                                    По своей сути рыбатекст является альтернативой традиционному lorem
                                                    ipsum,
                                                    который вызывает у
                                                    некторых людей недоумение при попытках прочитать рыбу текст.
                                                </div>
                                                <div className="arrow-wrapper-investor">
                                                    <a href="/"> <img src="/image/icon/right-arrow-blue.png"
                                                                      alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="question">
                    <div className="question-wrapper">
                        <div className="question-image">
                            <img src="/image/pictur/investor-bg.png" alt=""/>
                        </div>
                        <div className="question-text">
                            <div className="wrapper-text-question">
                                <div className="title-question-answer">Часто задаваемые вопросы</div>
                                <div className="items-block-question">

                                    {questions.map(question => <QuestionItem question={question}/>)}

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="startup">
                    <div className="container">
                        <div className="startup-wrapper">
                            <div className="title-startup">Стартапы</div>
                            <div className="subtitle-startup">
                                {settings.startup_text}
                            </div>
                            <div className="block-invis">
                                <div className="clear-block"/>
                                <div className="view-more">
                                    <Link href={'/startups'}>
                                        <a>Смотреть ещё</a>

                                    </Link>
                                </div>
                            </div>

                            <div className="wrapper-startup-column-group">
                                {businesses.map((business, index) => {
                                    if (index < 3) {
                                        return (
                                            <Link
                                                href={business.business_type.id === 1 ? `/startups/${business.slug}` : business.business_type.id === 2 ? `/businesses/${business.slug}` : `/commercial-apartments/${business.slug}`}>

                                                <div className="startups-column">
                                                    <div className="str-item-img">
                                                        <img src={business.image ? business.images[0] : ''} alt=""/>
                                                    </div>
                                                    <div className="str-item-date">{business.updated_at}</div>
                                                    <div className="str-item-price">
                                                        <span>20% в год</span><span>{business.currency.symbol} {business.price} мин</span>
                                                    </div>
                                                    <div className="str-item-text">
                                                        {business.title}

                                                    </div>
                                                    <div className="str-ite-btn">
                                                        <button>Контакты</button>
                                                    </div>
                                                    <div className="str-item-desc">{business.business_type.name}</div>
                                                </div>

                                            </Link>

                                        )
                                    }
                                })}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="banner-investor">
                    <div className="container">
                        <div className="banner-investor-wrapper">
                            <div className="banner-title-investor">
                                Начните инвестировать уже сегодня
                            </div>
                            <div className="banner-subtitle-investor">
                                {settings.start_text}
                            </div>
                            <div className="banner-btn-investor">
                                <button>Начать</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="social-block">
                    <div className="container">
                        <div className="social-block-wrapper">

                            <div className="social-main">
                                <div className="text-social">
                                    <div className="title-social">Мы в соцсетях</div>
                                </div>
                                <div className="description-social">
                                    {settings.socials_text}
                                </div>

                                <div className="social-icon">
                                    <div className="facebook">
                                        <img src="/image/icon/facebook.png" alt=""/>
                                    </div>
                                    <div className="twitter">
                                        <img src="/image/icon/twitter.png" alt=""/>
                                    </div>
                                    <div className="instagram">
                                        <img src="/image/icon/instagram.png" alt=""/>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </Layout>
    )
}
