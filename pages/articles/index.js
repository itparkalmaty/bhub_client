import Layout from "../../components/Layout";
import Head from "next/head";
import Sidebar from "../../components/Sidebar";
import Banner from "../../components/Banner";
import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchArticleCategories, fetchArticles} from "../../store/actions/article";
import ArticleItem from "../../components/items/article";

export default function Articles() {
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(fetchArticles())
        dispatch(fetchArticleCategories())

    },[])

    const articles = useSelector(state=>state.root.article.articles)
    const categories=  useSelector((state)=>state.root.article.categories)

    return (
        <Layout>

            <div className="busines-news-block">
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Бизнес новости</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt="" />
                            </div>
                            <div className="main-point">Бизнес новости</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="item-news-block">
                <div className="container">
                    <div className="wrapper-item-news-block">
                        <div className="column-left-pos">
                            {articles.map(article=><ArticleItem article={article} key={article.id}/>)}
                        </div>
                        <Sidebar categories={categories} type={'articles'}/>
                    </div>
                    <div className="pagination-block">
                        <div className="item-pagionation">
                            <ul>
                                <li className="active-pag">1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>...</li>
                                <li><img src="/image/icon/black-arrow-right.png" alt="" /></li>
                            </ul>
                        </div>
                    </div>
                    <Banner/>
                </div>
            </div>
        </Layout>
    )
}