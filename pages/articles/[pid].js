import Layout from "../../components/Layout";
import Sidebar from "../../components/Sidebar";
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, {useEffect} from "react";
import {GET_ARTICLE} from "../../store/types";
import {useDispatch, useSelector} from "react-redux";
import {fetchArticle, fetchArticleCategories} from "../../store/actions/article";

export default function Article(){
    const router = useRouter()
    const {pid} = router.query

    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(fetchArticle(pid))
        dispatch(fetchArticleCategories())
    },[pid])
    const article = useSelector((state)=>state.root.article.article)
    const categories=  useSelector((state)=>state.root.article.categories)
    return (
        <Layout>
            <Head>
                <title>{article.title}</title>
                <meta name={'description'} content={article.description}/>
            </Head>

            <div>
                <div className="busines-news-block">
                    <div className="container">
                        <div className="wrapper-busines-news-block">
                            <div className="title-busines-news">Карточка новости</div>
                            <div className="block-tag">
                                <div className="main-point">Главная</div>
                                <div className="span-arrow">
                                    <img src="/image/icon/green-arrow-right.png" alt="" />
                                </div>
                                <div className="main-point">Карточка новости</div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="item-news-block">
                    <div className="container">
                        <div className="wrapper-item-news-block">
                            <div className="column-left-pos card-pos">
                                <div className="main-pict-card-item">
                                    <img src={article.main_image} alt="" />
                                </div>
                                <div className="title-card-item" style={{width:'100%'}}>{article.name}</div>

                                <div className="description-card-item">
                                    {article.content}
                                </div>

                                <div className="statistic-block">
                                    <div className="date-publick">Дата публикации: {article.updated_at}</div>
                                    <div className="social-publick">
                                        <img src="/image/icon/shareplusoru.png" alt="" />
                                    </div>
                                </div>
                                <div className="next-news">
                                    <div className="data-news">Следующая новость</div>
                                    <div className="data-arrow">
                                        <img src="/image/icon/black-arrow-right.png" alt="" />
                                    </div>
                                </div>
                            </div>
                            <Sidebar categories={categories} type={'articles'}/>
                        </div>

                        <div className="banner-join-investor">
                            <div className="container">
                                <div className="wrapper-banner-join-investor">
                                    <div className="title-banner-join-investor">Присоединяйся к нашей профессиональной
                                        команде
                                    </div>
                                    <div className="desc-banner-join-investor">ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ
                                    </div>
                                    <div className="btn-banner-join">
                                        <button>Начать</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </Layout>
    )
}