import Layout from "../../components/Layout";
import {useRouter} from "next/router";
import React, {useEffect} from "react";
import Sidebar from "../../components/Sidebar";
import Banner from "../../components/Banner";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories, fetchFreelancer} from "../../store/actions/freelancer";

export default function Freelancer() {
    const router = useRouter()
    const {pid} = router.query

    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(fetchFreelancer(pid))
        dispatch(fetchCategories())

    },[pid])
    const freelancer = useSelector(state=>state.root.freelance.freelancer)
    const categories = useSelector(state=>state.root.freelance.categories)
    const user = useSelector(state=>state.root.user.user)
    console.log(freelancer)
    return (
        <Layout>
            <div className="busines-news-block startup-single-block"
                 style={{backgroundImage: 'url(/image/startup.png)'}}>
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Карточка фрилансера</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt=""/>
                            </div>
                            <div className="main-point">Карточка фрилансера</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="items-news-block">
                <div className="container">
                    <div className="wrapper-item-news-block">
                        <div className="column-left-pos card-pos">
                            <div className="main_background-image" style={{backgroundImage:`url(${freelancer.avatar})`}}>
                                <div className="id-startup">
                                    {freelancer.id}
                                </div>
                            </div>
                            <div className="title-text" style={{width: '100%'}}>
                                {freelancer.name}
                            </div>
                            <div className="subtitle-text" style={{width: '100%'}}>
                                {freelancer.nickname}
                            </div>
                            <div className="characteristics-text" style={{width: '100%'}}>
                                {freelancer.description}
                            </div>
                            <div className="main-flex">

                                <div className="rating">
                                    Рейтинг:
                                    <div>
                                        <i className="fas fa-star"/>
                                        <i className="fas fa-star"/>
                                        <i className="fas fa-star"/>
                                        <i className="fas fa-star"/>
                                        <i className="fas fa-star"/>
                                    </div>
                                </div>
                            </div>
                            <div className="price-buttons-price">
                                <div className="price">
                                    <div className="price-status">
                                        Ставка <br/>
                                    </div>
                                    {freelancer.price && freelancer.price.toLocaleString()} {freelancer.currency && freelancer.currency.symbol}

                                </div>
                                <button className={'contact-button'}>{user.role_id == 2 ? freelancer.phone :'Показать контакты'}</button>

                            </div>


                        </div>
                        <Sidebar categories={categories} type={'freelancers'}/>
                    </div>
                    <div className="banner_block">
                        <Banner/>
                    </div>
                </div>
            </div>
        </Layout>
    )
}