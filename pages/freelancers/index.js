import Layout from "../../components/Layout";
import Sidebar from "../../components/Sidebar";
import React, {useCallback, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories, fetchFreelancers} from "../../store/actions/freelancer";
import FreelancerItem from "../../components/items/freelancer";

export default function Freelancers(){
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(fetchFreelancers())
        dispatch(fetchCategories())
    },[])

    const freelancers = useSelector((state)=> state.root.freelance.freelancers)
    const categories = useSelector(state=>state.root.freelance.categories)
    return (
        <Layout>
            <div>
                <div className="frilanser-page-news-block">
                    <div className="container">
                        <div className="wrapper-busines-news-block">
                            <div className="title-busines-news">Фрилансеры</div>
                            <div className="block-tag">
                                <div className="main-point">Главная</div>
                                <div className="span-arrow">
                                    <img src="/image/icon/green-arrow-right.png" alt="" />
                                </div>
                                <div className="main-point">Фрилансеры</div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="item-news-block">
                    <div className="container">
                        <div className="wrapper-item-news-block">
                            <div className="column-left-pos left-dop-block">
                                {freelancers && freelancers.map(freelancer=><FreelancerItem freelancer={freelancer} key={freelancer.id}/>)}
                            </div>
                            <Sidebar categories={categories} type={'freelancers'}/>
                        </div>
                        <div className="pagination-block">
                            <div className="item-pagionation">
                                <ul>
                                    <li className="active-pag">1</li>
                                    <li>2</li>
                                    <li>3</li>
                                    <li>...</li>
                                    <li><img src="/image/icon/black-arrow-right.png" alt="" /></li>
                                </ul>
                            </div>
                        </div>
                        <div className="banner-join-investor">
                            <div className="container">
                                <div className="wrapper-banner-join-investor">
                                    <div className="title-banner-join-investor">Присоединяйся к нашей профессиональной
                                        команде
                                    </div>
                                    <div className="desc-banner-join-investor">ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ</div>
                                    <div className="btn-banner-join">
                                        <button>Начать</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </Layout>
    )
}