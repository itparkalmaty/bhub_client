import Sidebar from "../../components/Sidebar";
import Layout from "../../components/Layout";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {fetchBusinesses, fetchCategories} from "../../store/actions/business";
import BusinessItem from "../../components/items/business";
import BusinessList from "../../components/list/BusinessList";

export default function Startups(){
    return (
        <Layout>
            <div className="startup-news-block">
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Инвестпроекты</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt="" />
                            </div>
                            <div className="main-point">Инвестпроекты</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="item-news-block">
                <BusinessList id={1}/>
            </div>
        </Layout>
    )
}