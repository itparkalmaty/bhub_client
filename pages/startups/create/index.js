import Layout from "../../../components/Layout";
import React, {useEffect} from "react";
import BusinessItem from "../../../components/items/business";
import Sidebar from "../../../components/Sidebar";
import ImageUploading from 'react-images-uploading';
import {useDispatch, useSelector} from "react-redux";
import {
    fetchAllCategories,
    fetchCities,
    fetchCurrencies,
    fetchTypes,
    storeBusiness
} from "../../../store/actions/business";
import Swal from "sweetalert2";

export default function StartupCreate() {
    const [images, setImages] = React.useState([]);
    const maxNumber = 69;

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchCurrencies())
        dispatch(fetchCities())
        dispatch(fetchTypes(1))
        dispatch(fetchAllCategories(1))
    }, [])
    const [form, setForm] = React.useState({
        title: '',
        subtitle: '',
        videos: '',
        images: '',
        min_price: '',
        phone: '',
        category_id: '',
        state: '',
        details: '',
        idea: '',
        business_type_id: '',
        currency_id: '',
        city_id: '',
        document: '',
        percentage: ''
    })


    const cities = useSelector(state => state.root.business.cities)
    const types = useSelector(state => state.root.business.types)
    const currencies = useSelector(state => state.root.business.currencies)
    const categories = useSelector(state => state.root.business.categories)
    const changeFileInput = e => {
        setForm({...form, document: e.target.files[0]})
    }
    const changeInputEvent = event => {
        setForm({...form, [event.target.name]: event.target.value})
        console.log(form)
    }
    const onChange = (imageList, addUpdateIndex) => {
        // data for submit
        let imageArray = []
        imageList.forEach(image => {
            imageArray.push(image.file)
        })
        setImages(imageList);
        setForm({...form, images: imageArray})
        console.log(form)

    };

    return (
        <Layout>
            <div className="startup-news-block">
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Страница заполнения стартапа</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt=""/>
                            </div>
                            <div className="main-point">Страница заполнения стартапа</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="item-news-block">
                <div className="container">
                    <div className="wrapper-item-news-block">
                        <div className="column-left-pos main-editor">
                            <div className="form-group">
                                <div className="main-label">
                                    Заголовок
                                </div>
                                <input type="text" className="form-control" placeholder="Пример заголовка"
                                       name={'title'} onChange={changeInputEvent}/>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Под загаловок
                                </div>
                                <input type="text" className="form-control" placeholder="Пример заголовка"
                                       name={'subtitle'} onChange={changeInputEvent}/>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Телефон
                                </div>
                                <input type="text" className="form-control" placeholder="+7 (707) 654-32-10"
                                       name={'phone'} onChange={changeInputEvent}/>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Тип инвестпроекта
                                </div>
                                <select name='business_type_id' onChange={(e) => {
                                    changeInputEvent(e)
                                    dispatch(fetchAllCategories(e.target.value))
                                }} className={'form-control'}>
                                    <option value="">Нету</option>
                                    {types.map(city => <option value={city.id}>{city.name}</option>)}
                                </select>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Категория
                                </div>
                                <select name='category_id' onChange={(e) => {
                                    changeInputEvent(e)
                                }} className={'form-control'}>
                                    <option value="">Нету</option>
                                    {categories.map(city => <option value={city.id}>{city.name}</option>)}
                                </select>
                            </div>

                            <div className="form-group w-50">
                                <div className="main-label">
                                    Город
                                </div>
                                <select name='city_id' onChange={changeInputEvent} className={'form-control'}>
                                    <option value="">Нету</option>
                                    {cities.map(city => <option value={city.id}>{city.name}</option>)}
                                </select>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Валюта
                                </div>
                                <select name='currency_id' onChange={changeInputEvent} className={'form-control'}>
                                    <option value="">Нету</option>
                                    {currencies.map(city => <option value={city.id}>{city.name}</option>)}
                                </select>
                            </div>

                            <div className="form-group">
                                <div className="main-label">
                                    Цена
                                </div>
                                <input type="number" className="form-control" placeholder="2999" name={'min_price'}
                                       onChange={changeInputEvent}/>
                            </div>
                            {/*<div className="form-group">*/}
                            {/*    <div className="main-label">*/}
                            {/*        Процент в год*/}
                            {/*    </div>*/}
                            {/*    <input type="number" className="form-control" placeholder="10"*/}
                            {/*           name={'percentage'} onChange={changeInputEvent}/>*/}
                            {/*</div>*/}

                            <div className="form-group">
                                <div className="main-label">
                                    Ссылка на видео (Youtube)
                                </div>
                                <input type="text" className="form-control" placeholder="Ссылка на видео"
                                       name={'videos'} onChange={changeInputEvent}/>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Презентация
                                </div>
                                <input type="file" className="form-control" placeholder="Ссылка на видео"
                                       name={'document'} onChange={changeFileInput}/>
                            </div>
                            <div className="form-group w-100">
                                <div className="main-label">
                                    Идея
                                </div>
                                <textarea rows={5} className="form-control" placeholder="Ваше описание" name={'details'}
                                          onChange={changeInputEvent}/>
                            </div>

                            <div className="form-group w-100">
                                <div className="main-label">
                                    Описание
                                </div>
                                <textarea rows={5} className="form-control" placeholder="Ваше описание" name={'idea'}
                                          onChange={changeInputEvent}/>
                            </div>

                            {/*<div className="form-group w-100">*/}
                            {/*    <div className="main-label">*/}p
                            {/*        Состояние*/}
                            {/*    </div>*/}
                            {/*    <textarea rows={5} className="form-control" placeholder="Ваше описание" name={'state'}*/}
                            {/*              onChange={changeInputEvent}/>*/}
                            {/*</div>*/}
                            <div className="form-group w-100">
                                <ImageUploading
                                    multiple
                                    value={images}
                                    onChange={onChange}
                                    maxNumber={maxNumber}
                                    dataURLKey="data_url"
                                >
                                    {({
                                          imageList,
                                          onImageUpload,
                                          onImageRemoveAll,
                                          onImageUpdate,
                                          onImageRemove,
                                          isDragging,
                                          dragProps,
                                      }) => (
                                        // write your building UI
                                        <div className="upload__image-wrapper">
                                            <button
                                                className={'add_button'}
                                                style={isDragging ? {color: 'red'} : undefined}
                                                onClick={onImageUpload}
                                                {...dragProps}
                                            >
                                                Добавить фото
                                            </button>
                                            &nbsp;
                                            <button className={'delete-all'} onClick={onImageRemoveAll}>Удалить все
                                                фото
                                            </button>
                                            <div className="images">
                                                {imageList.map((image, index) => (
                                                    <div key={index} className="image-item">
                                                        <img src={image['data_url']} alt="" width="100"/>
                                                        <div className="image-item__btn-wrapper">
                                                            <button className={'update-btn'}
                                                                    onClick={() => onImageUpdate(index)}>Изменить
                                                            </button>
                                                            <button className={'delete-btn'}
                                                                    onClick={() => onImageRemove(index)}>Удалить
                                                            </button>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        </div>
                                    )}
                                </ImageUploading>
                            </div>
                            <div className="form-group">
                                <button className={'submit-btn-form'}
                                        onClick={() => dispatch(storeBusiness(form, form.business_type_id))}>Сохранить
                                </button>
                            </div>


                        </div>
                        <Sidebar/>


                    </div>
                </div>
            </div>
        </Layout>
    )
}
