import Layout from "../../components/Layout";
import React, {useEffect} from "react";
import Sidebar from "../../components/Sidebar";
import Banner from "../../components/Banner";
import {useRouter} from "next/router";
import {useDispatch, useSelector} from "react-redux";
import {fetchBusiness, fetchCategories} from "../../store/actions/business";
import BusinessComponent from "../../components/show/BusinessComponent";
import Loader from "react-loader-spinner";
export default function Startup() {
    const router = useRouter()
    const {pid} = router.query

    const dispatch = useDispatch()

    const user = useSelector(state=>state.root.user.user)
    useEffect(() => {
        dispatch(fetchBusiness(pid,user.id))
        dispatch(fetchCategories(1))
    }, [pid, router, user])

    const business = useSelector(state => state.root.business.mainBusiness)
    const categories = useSelector(state => state.root.business.categories)
    const loading = useSelector(state => state.root.app.loading)

    if (loading || Object.keys(business).length === 0)
        return (
            <Layout>
                <div className="busines-news-block startup-single-block"
                     style={{backgroundImage: 'url(/image/startup.png)'}}>
                    <div className="container">
                        <div className="wrapper-busines-news-block">
                            <div className="title-busines-news">Карточка стартапа</div>
                            <div className="block-tag">
                                <div className="main-point">Главная</div>
                                <div className="span-arrow">
                                    <img src="/image/icon/green-arrow-right.png" alt=""/>
                                </div>
                                <div className="main-point">Карточка стартапа</div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="container main-new-flex">
                    <Loader  type="Puff"
                             color="#00D664"
                             height={100}
                             width={100}
                              />
                </div>
            </Layout>
        )
    return (
        <Layout>
            <div className="busines-news-block startup-single-block"
                 style={{backgroundImage: 'url(/image/startup.png)'}}>
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Карточка стартапа</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt=""/>
                            </div>
                            <div className="main-point">Карточка стартапа</div>
                        </div>

                    </div>
                </div>
            </div>


            <BusinessComponent business={business} categories={categories}/>
        </Layout>
    )
}