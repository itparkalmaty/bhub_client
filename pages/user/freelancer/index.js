import Layout from "../../../components/Layout";
import React, {useEffect} from "react";
import UserBar from "../../../components/user/UserBar";
import {useDispatch, useSelector} from "react-redux";
import {fetchCurrencies} from "../../../store/actions/business";
import {storeFreelancerAccount, updateFreelancer} from "../../../store/actions/user";

export default function FreelancerUser() {
    const user = useSelector(state=>state.root.user.user)
    const [form,setForm] = React.useState({
        description: user.freelancer ? user.freelancer.description : '',
        verification_image:'',
        avatar:'',
        price:user.freelancer ? user.freelancer.price : '',
        currency_id:user.freelancer ? user.freelancer.currency.id : '',
        nickname: user.freelancer ? user.freelancer.nickname : '',
        phone: user.freelancer ? user.freelancer.phone : ''
    })

    const dispatch = useDispatch()
    useEffect(()=> {
        dispatch(fetchCurrencies())
    },[])
    const currencies = useSelector(state=>state.root.business.currencies)

    const changeEventHandler = (e) => {
        setForm({...form, [e.target.name]: e.target.value})
        console.log(form)
    }

    const changeFileEvent = (e)=>{
        setForm({...form,[e.target.name]: e.target.files[0]})
    }
    return (
        <Layout>
            <div className="busines-news-block">
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Кабинет</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt=""/>
                            </div>
                            <div className="main-point">Кабинет</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="item-news-block">
                <div className="container">
                    <div className="wrapper-item-news-block">
                        <UserBar/>

                        <div className="main-block-user">
                            <div className="form-group w-100">
                                <div className="main-label">
                                    Описание ваших услуг
                                </div>
                                <textarea name="description" id="" className={'form-control'} rows="5" onChange={changeEventHandler}>
                                    {form.description}
                                </textarea>
                            </div>
                            <div className="form-group w-100">
                                <div className="main-label">
                                    Имя пользователя
                                </div>
                                <input type="text" name={'nickname'} value={form.nickname} className={'form-control'} onChange={changeEventHandler}/>
                            </div>
                            <div className="form-group w-100">
                                <div className="main-label">
                                    Номер телефона
                                </div>
                                <input type="text" name={'phone'} value={form.phone} className={'form-control'} onChange={changeEventHandler}/>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Цена
                                </div>
                                <input type="number" name={'price'} value={form.price} className={'form-control'} onChange={changeEventHandler}/>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Валюта
                                </div>
                                <select name="currency_id" id="" className="form-control" onChange={changeEventHandler}>
                                    <option value="">Нету</option>
                                    {currencies.map(currency=> <option value={currency.id}>{currency.name}</option>)}
                                </select>
                            </div>
                            <div className="form-group w-100">

                                <div className="main-label">
                                    Аватар
                                </div>
                                <input type="file" className="form-control" onChange={changeFileEvent} name={'avatar'}/>
                            </div>
                            {!user.freelancer &&
                            <div className="form-group w-100">
                                <div className="main-label">
                                    Фото верификации
                                </div>
                                <input type="file" className={'form-control'} onChange={changeFileEvent} name={'verification_image'}/>
                            </div>
                            }
                            <div className="form-group w-100">
                                <button className="create-btn w-50" onClick={user.freelancer ? ()=>dispatch(updateFreelancer(form)) : ()=>dispatch(storeFreelancerAccount(form))}>
                                    Обновить страницу
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </Layout>
    )
}