import Layout from "../../components/Layout";
import {useDispatch, useSelector} from "react-redux";
import {fetchViews, logout} from "../../store/actions/user";
import UserBar from "../../components/user/UserBar";
import React, {useEffect} from "react";
import ViewItem from "../../components/items/views";

export default function User() {
    const dispatch = useDispatch()
    useEffect(()=>dispatch(fetchViews()),[])
    const businesses = useSelector(state=>state.root.user.views)
    console.log(businesses)
    return (
        <Layout>
            <div className="busines-news-block">
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Кабинет</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt="" />
                            </div>
                            <div className="main-point">Кабинет</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="item-news-block">
                <div className="container">
                    <div className="wrapper-item-news-block">
                        <UserBar/>
                        <div className="column-left-pos">
                            {businesses && businesses.map(business=><ViewItem view={business}/>)}

                        </div>
                    </div>
                    <div className="pagination-block">
                        <div className="item-pagionation">
                            <ul>
                                <li className="active-pag">1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>...</li>
                                <li><img src="/image/icon/black-arrow-right.png" alt="" /></li>
                            </ul>
                        </div>
                    </div>
                    <div className="banner-join-investor">
                        <div className="container">
                            <div className="wrapper-banner-join-investor">
                                <div className="title-banner-join-investor">Присоединяйся к нашей профессиональной
                                    команде
                                </div>
                                <div className="desc-banner-join-investor">ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ</div>
                                <div className="btn-banner-join">
                                    <button>Начать</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </Layout>
    )
}