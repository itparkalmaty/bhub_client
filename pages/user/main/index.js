import Layout from "../../../components/Layout";
import React from "react";
import UserBar from "../../../components/user/UserBar";
import {useDispatch, useSelector} from "react-redux";
import {updateUser} from "../../../store/actions/user";
import Link from "next/link";

export default function UserData() {
    const user = useSelector(state=>state.root.user.user)
    const [form,setForm] = React.useState({
        email: user.email,
        name: user.name,
        password:'',
    })

    const changeInputEvent = e => {
        setForm({...form,[e.target.name]:e.target.value})
    }
    const dispatch = useDispatch()
    return (
        <Layout>
            <div className="busines-news-block">
                <div className="container">
                    <div className="wrapper-busines-news-block">
                        <div className="title-busines-news">Кабинет</div>
                        <div className="block-tag">
                            <div className="main-point">Главная</div>
                            <div className="span-arrow">
                                <img src="/image/icon/green-arrow-right.png" alt=""/>
                            </div>
                            <div className="main-point">Кабинет</div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="item-news-block">
                <div className="container">
                    <div className="wrapper-item-news-block">
                        <UserBar/>
                        <div className="main-block-user">
                            <div className="form-group">
                                <div className="main-label">
                                    Ваша почта
                                </div>
                                <input type="email" className="form-control" placeholder="your@email.com"
                                       name={'email'} onChange={changeInputEvent} value={form.email}/>
                            </div>
                            <div className="form-group">
                                <div className="main-label">
                                    Ваше имя
                                </div>
                                <input type="text" className="form-control" placeholder="your@email.com"
                                       name={'name'} onChange={changeInputEvent} value={form.name}/>
                            </div>

                            <div className="form-group w-100">
                                <div className="main-label">
                                    Ваш пароль
                                </div>
                                <input type="password" className="form-control" placeholder=""
                                       name={'password'} onChange={changeInputEvent} value={form.password}/>
                            </div>
                            <div className="form-group w-100 flex-wrap" style={{display:'flex',justifyContent:'space-between'}}>
                                <button className="create-btn" onClick={()=>dispatch(updateUser(form))}>Сохранить</button>
                                <Link href={'/user/freelancer'}>
                                    <button className="create-btn">{user.freelancer ? 'Аккаунт фрилансера' : 'Создать аккаунт фрилансера'}</button>

                                </Link>
                                <button className="create-btn" onClick={user.role_id != 2 ? ()=>dispatch(updateUser({
                                    role_id:2
                                })) : console.log('main')}>{user.role_id == 2 ? 'Вы уже инвестор' : 'Стать инвестором'}</button>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}