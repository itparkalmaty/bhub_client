import Sidebar from "../Sidebar";
import Banner from "../Banner";
import Layout from "../Layout";
import React from "react";
import {useSelector} from "react-redux";

export default function BusinessComponent(props) {
    const business = props.business
    const categories = props.categories
    const user = useSelector(state => state.root.user.user)
    return <div className="items-news-block">
        <div className="container">
            <div className="wrapper-item-news-block">
                <div className="column-left-pos card-pos">
                    <div className="main_background-image" style={{backgroundImage: `url(${business.images ? business.images[0] : ''})`}}>
                        <div className="id-startup">
                            {business.id}
                        </div>
                    </div>
                    <div className="title-text" style={{width: '100%'}}>
                        {business.title}
                    </div>
                    <div className="subtitle-text" style={{width: '100%'}}>
                        {business.subtitle}
                    </div>
                    <div className="characteristics-text" style={{width: '100%'}}>
                        <span>{business.city.country.name}:</span> город {business.city.name} <br/>
                        <span>Отрасль:</span> {business.category.name} <br/>
                        <span>Стадия проекта:</span> {business.business_type.name} <br/>
                    </div>
                    <div className="main-flex">
                        <div className="date-time">
                            Дата последнего изменения: {business.updated_at}
                        </div>
                        <div className="rating">
                            Рейтинг:
                            <div>
                                <i className="fas fa-star"/>
                                <i className="fas fa-star"/>
                                <i className="fas fa-star"/>
                                <i className="fas fa-star"/>
                                <i className="fas fa-star"/>
                            </div>
                        </div>
                    </div>
                    <div className="price-buttons-price">
                        <div className="price">
                            {business.currency.symbol} {business.price.toLocaleString()} мин
                            <div className="price-status">
                                Ищет инвестиции
                            </div>
                        </div>
                        <a href={business.document} target={'_blank'}>
                            <button className={'green-button'}>Показать документ</button>
                        </a>
                        <button className={'contact-button'}>{user.role_id == 2 ? business.phone : 'Показать контакты'}</button>

                    </div>
                    {business.idea && <div className="idea_container">
                        <div className="idea_title">
                            Идея
                        </div>
                        <div className="idea_description">
                            {business.idea}
                        </div>
                    </div>}
                    {business.state && <div className="idea_container">
                        <div className="idea_title">
                            Состояние
                        </div>
                        <div className="idea_description">
                            {business.state}
                        </div>
                    </div>}
                    {business.details && <div className="idea_container">
                        <div className="idea_title">
                            Проблема или возможность
                        </div>
                        <div className="idea_description">
                            {business.details}
                        </div>
                    </div>}
                    <div className="team_title">
                        Видео о продукте
                    </div>
                    {business.videos &&
                    <iframe width="100%" height="315" src={business.videos}
                            title="YouTube video player" frameBorder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen/>}
                </div>
                <Sidebar categories={categories} type={'business'} business_type={business.business_type.id}/>
            </div>
            <div className="banner_block">
                <Banner/>
            </div>
        </div>
    </div>

}