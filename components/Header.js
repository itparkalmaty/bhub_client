import Link from "next/link";
import Head from "next/head";
import Login from "./modals/Login";
import React, {useEffect, useState} from "react";
import BusinessTypes from "./modals/BusinessTypes";
import {useSelector} from "react-redux";
import Register from "./modals/Register";

export default function Header() {
    const [modalLogin, setModalLogin] = useState(false);
    const [modalTypes, setModalTypes] = useState(false);
    const [modalRegister, setModalRegister] = useState(false);
    const user = useSelector((state) => state.root.user.user)
    const [menu, setMenu] = React.useState(false)
    const closeLogin = () => setModalLogin(false)
    const [header,setHeader] = React.useState(false)

    useEffect(() => {
        window.addEventListener('scroll', e => {

            if (window.pageYOffset > 355){
                setHeader(true)
            }else{
                setHeader(false)
            }
        })
    }, []);

    return (
        <div id="header" className={`${header ? 'active' : ''}`}>
            <Head>
                <meta name="viewport"
                      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
                <title>main</title>
            </Head>
            <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
                  integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
                  crossOrigin="anonymous"/>

            <link rel="preconnect" href="https://fonts.gstatic.com"/>
            <link
                href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap"
                rel="stylesheet"/>
            <div className="container">
                <div className="wrapper-header">
                    <Link href="/">
                        <a>
                            <div className="logo">
                                <img src="/image/Logo.png" alt=""/>
                            </div>
                        </a>
                    </Link>

                    <div className="nav-menu">
                        <div className="nav-item">
                            <ul>
                                <Link href={'/'}>
                                    <a>
                                        <li>Главная</li>
                                    </a>
                                </Link>
                                {/*<Link href={'/articles'}>*/}
                                {/*    <a>*/}
                                {/*        <li>Бизнес новости</li>*/}
                                {/*    </a>*/}
                                {/*</Link>*/}
                                <Link href={'/businesses'}>
                                    <li>Продажа бизнеса</li>
                                </Link>
                                <Link href={'/startups'}>
                                    <li>Инвестпроекты</li>
                                </Link>
                                {modalTypes && <BusinessTypes/>}
                                <Link href={'/freelancers'}>
                                    <a>
                                        <li>Фрилансеры</li>
                                    </a>
                                </Link>
                            </ul>
                        </div>
                    </div>
                    <div className="search">
                        <img src="/image/icon/search.png" alt=""/>
                    </div>
                    <div className="join-group">
                        <button className={'menu-button'} onClick={() => setMenu(!menu)}><i
                            className={`${menu ? 'fas fa-times' : 'fas fa-bars'}  `}/></button>
                        {user.name ?
                            <Link href={'/user/main'}>
                                <button>{user.name}</button>
                            </Link> :
                            <button className={`login-button`} onClick={() => setModalLogin(!modalLogin)}>Войти</button>
                        }
                        {modalLogin && <Login closeLogin={() => setModalLogin(false)} openRegister={() => {
                            setModalLogin(false)
                            setModalRegister(true)

                        }}/>}
                        {modalRegister && <Register closeLogin={() => setModalRegister(false)} openLogin={() => {
                            setModalRegister(false)
                            setModalLogin(true)
                        }}/>}
                    </div>

                </div>
            </div>
            <div className={`nav-menu-mobile ${menu ? 'active' : ''}`}>
                <div className="nav-item">
                    <ul>
                        <Link href={'/'}>
                            <a>
                                <li>Главная</li>
                            </a>
                        </Link>
                        {/*<Link href={'/articles'}>*/}
                        {/*    <a>*/}
                        {/*        <li>Бизнес новости</li>*/}
                        {/*    </a>*/}
                        {/*</Link>*/}
                        <Link href={'/businesses'}>
                            <li>Бизнесы</li>
                        </Link>
                        <Link href={'/startups'}>
                            <li>Стартапы</li>
                        </Link>
                        {modalTypes && <BusinessTypes/>}
                        <Link href={'/freelancers'}>
                            <a>
                                <li>Фрилансеры</li>
                            </a>
                        </Link>
                    </ul>
                </div>
            </div>
        </div>

    )
}