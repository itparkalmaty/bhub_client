import Link from "next/link";
import {useDispatch, useSelector} from "react-redux";
import {chooseBusiness} from "../../store/actions/business";

export default function BusinessItem(props) {
    const user = useSelector(state=>state.root.user.user)
    return (
        <Link href={props.id === 1 ? `/startups/${props.business.slug}` : props.id === 2 ? `/businesses/${props.business.slug}` : `/commercial-apartments/${props.business.slug}` }  >
            <div className="startups-column">
                <div className="str-item-img">
                    <img src={props.business.images ? props.business.images[0] : ''} alt="" />
                </div>
                <div className="str-item-date">{props.business.updated_at}</div>
                <div className="str-item-price">

                    <span>{props.business.currency.symbol} {props.business.price.toLocaleString()}  {props.business.percentage ? props.business.percentage+"%" : ''}</span>
                </div>
                <div className="str-item-text">
                    {props.business.title}
                </div>
                <div className="str-ite-btn">
                    <button>{user.role_id != 2 ? 'Контакты' : props.business.phone}</button>
                </div>
                <div className="str-item-desc" >{props.business.business_type.name}</div>
            </div>
        </Link>

    )
}