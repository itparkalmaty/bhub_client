import Link from "next/link";

export default function FreelancerItem(props){
    return (
        <Link href={`/freelancers/${props.freelancer.nickname}`}>
            <div className="column-frilans">
                <div className="person-item">
                    <div className="pict-person">
                        <img src={props.freelancer.avatar} alt="" />
                    </div>
                    <div className="name-person">{props.freelancer.user.name}</div>
                </div>
                <div className="description-person">
                    {props.freelancer.description}
                </div>
                <div className="skills-person">
                    {props.freelancer.categories.map(category=><p>{category.name}</p>)}
                </div>
                <div className="price-person">
                    <p>От {props.freelancer.price} {props.freelancer.currency.symbol}/час </p>
                    <p className="arrow-person">
                        <img src="/image/icon/right-arrow-blue.png" alt="" />
                    </p>
                </div>
            </div>
        </Link>
    )
}