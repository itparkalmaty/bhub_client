import Link from "next/link";
import React from "react";
import {useSelector} from "react-redux";

export default function UserBusinessItem(props) {
    const user = useSelector(state=>state.root.user.user)
    return (
        <Link href={`/${props.business.business_type.link}/${props.business.slug}`}>
            <div className="startups-column main-column">
                <div className="str-item-img">
                    <img src={props.business.images ? props.business.images[0] : ''} alt="" />
                </div>
                <div className="str-item-date">{props.business.updated_at}</div>
                <div className="str-item-price">
                    <span>{props.business.currency.symbol} {props.business.price.toLocaleString()} </span>
                </div>
                <div className="str-item-text">
                    {props.business.title}
                </div>
                <div className="str-ite-btn">
                    <button>{user.role_id == 2 ? props.business.phone : 'Контакты' }</button>
                </div>
                <div className="str-item-desc">{props.business.business_type.name}</div>
            </div>
        </Link>

    )
}