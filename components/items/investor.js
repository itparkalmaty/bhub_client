import React from "react";

export default function InvestorItem(props) {
    return (
        <div className="startup-column">
            <div className="item-startup-prof">
                <div className="item-icon-startup">
                    <img src={props.investor.image} alt="" class='investor-image'/>
                </div>
                <div className="item-name-sartup">{props.investor.name}</div>
            </div>
            <div className="description-startup-prof">
                {props.investor.description}
            </div>
            <div className="arrow-wrapper-investor">
                <a href="/"> <img src="/image/icon/right-arrow-blue.png"
                                  alt=""/></a>
            </div>
        </div>

    )
}