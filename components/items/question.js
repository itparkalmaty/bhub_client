import React from "react";

export default function QuestionItem(props) {
    const [active,setActive] = React.useState(false)
    return(
        <div className="item-question" onClick={()=>setActive(!active)}>
            <div className={`name-question ${active && `action-question`}`}>
                <span/> {props.question.question}
            </div>
            <div className={`name-answer ${!active && 'hide'}`}>
                {props.question.answer}
            </div>
        </div>
    )
}