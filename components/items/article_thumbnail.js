import Link from "next/link";

export default function ThumbnailItem(props) {
    return (
        <Link href={`/articles/${props.article.slug}`}>
            <div className="item-type-news-column-search">
                <div className="pict-item-type-search">
                    <img src={props.article.main_image} alt="" />
                </div>
                <div className="text-box-search">
                    <div className="title-text-box">{props.article.name}</div>
                    <div className="date-text-box">{props.article.updated_at}</div>
                </div>

            </div>
        </Link>

    )
}