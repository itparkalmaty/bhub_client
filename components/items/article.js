import Link from "next/link";

export default function ArticleItem(props) {
    return (
        <div className="column-item-news">
            <div className="item-news-pict">
                <img src={props.article.main_image} alt="" />
            </div>
            <div className="str-item-date">{props.article.created_at}</div>
            <div className="title-item-busines">{props.article.name}
            </div>
            <div className="desc-item-busines">
                {props.article.description}
            </div>
            <Link href={`/articles/${props.article.slug}`}>
                <div className="read-more">
                    <div className="read-block">Читать далее</div>
                    <div className="read-arrow"><img src="/image/icon/right-arrow-blue.png" alt="" />
                    </div>
                </div>
            </Link>

        </div>

    )
}