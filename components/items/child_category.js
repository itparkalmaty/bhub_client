import {useDispatch} from "react-redux";
import {fetchByBusinessCategory} from "../../store/actions/business";

export default function ChildCategory(props){
    const dispatch = useDispatch()

    return (
        <div className={'child-category-single'} onClick={()=>dispatch(fetchByBusinessCategory(props.category.link))}>
            <div className="name-category">{props.category.name}</div>
            <div className="num-category">{props.category.count}</div>
        </div>
    )
}