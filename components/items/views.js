import React from "react";
import Link from "next/link";
import {useSelector} from "react-redux";

export default function ViewItem(props) {
    const user = useSelector(state=>state.root.user.user)
    return (
        <Link href={`/${props.view.business.business_type.link}/${props.view.business.slug}`}>
            <div className="startups-column">
                <div className="str-item-img">
                    <img src={props.biew.business.images ? props.view.business.images[0] : ''} alt="" />
                </div>
                <div className="str-item-date">{props.view.created_at}</div>
                <div className="str-item-price">
                    <span>{props.view.business.currency.symbol} {props.view.business.price.toLocaleString()} мин</span>
                </div>
                <div className="str-item-text">
                    {props.view.business.title}
                </div>
                <div className="str-ite-btn">
                    <button>{user.role_id == 2 ? props.view.business.phone : 'Контакты'}</button>
                </div>
                <div className="str-item-desc">{props.view.business.business_type.name}</div>
            </div>
        </Link>

    )
}