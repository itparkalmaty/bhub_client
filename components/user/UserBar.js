import {logout} from "../../store/actions/user";
import {useDispatch, useSelector} from "react-redux";
import Link from "next/link";

export default function UserBar() {
    const dispatch = useDispatch()
    const user = useSelector(state => state.root.user.user)
    return (
        <div className="column-right-pos mg-right">
            <div className="menu-cabinet">
                <Link href={'/user/main'}>
                    <div className="menu-link">
                        <div>
                            <i className="fas fa-user"/>
                            Личные данные
                        </div>
                        <i className="fas fa-chevron-right"/>
                    </div>
                </Link>

                <Link href={'/user/startups-businesses'}>
                    <div className="menu-link">
                        <div>
                            <i className="fas fa-passport"/>
                            Инвестпроекты
                        </div>
                        <i className="fas fa-chevron-right"/>
                    </div>
                </Link>
                <Link href={'/user/businesses'}>
                    <div className="menu-link">
                        <div>
                            <i className="fas fa-passport"/>
                             Бизнесы
                        </div>
                        <i className="fas fa-chevron-right"/>
                    </div>
                </Link>
                <Link href={'/user'}>
                    <div className="menu-link">
                        <div>
                            <i className="far fa-heart"/>
                            Просмотренные
                        </div>
                        <i className="fas fa-chevron-right"/>
                    </div>
                </Link>
                <div className="menu-link" onClick={() => dispatch(logout())}>
                    <div>
                        <i className="fas fa-sign-out-alt"/>
                        Выйти
                    </div>
                    <i className="fas fa-chevron-right"/>
                </div>
            </div>
            <div className="banner-search">
                <img src="/image/pictur/Banner.png" alt=""/>
            </div>
        </div>

    )
}