import ArticleItem from "../items/article";
import Sidebar from "../Sidebar";
import Banner from "../Banner";
import BusinessItem from "../items/business";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {fetchBusinesses, fetchCategories, fetchTypes} from "../../store/actions/business";

export default function BusinessList(props) {
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(fetchBusinesses(props.id))
        dispatch(fetchTypes(props.id))
    },[])

    const businesses = useSelector(state=>state.root.business.businesses)
    const categories = useSelector(state => state.root.business.types)

    return <div className="container">
        <div className="wrapper-item-news-block">
            <div className="column-left-pos">
                {businesses.length ? businesses.map(business => <BusinessItem key={business.id} id={props.id} business={business}/>) : ''}
            </div>
            <Sidebar categories={categories} type={'business'} business_type={props.id}/>
        </div>
        <div className="pagination-block">
            <div className="item-pagionation">
                <ul>
                    <li className="active-pag">1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>...</li>
                    <li><img src="/image/icon/black-arrow-right.png" alt=""/></li>
                </ul>
            </div>
        </div>
        <div className="banner-join-investor">
            <div className="container">
                <div className="wrapper-banner-join-investor">
                    <div className="title-banner-join-investor">Присоединяйся к нашей профессиональной команде</div>
                    <div className="desc-banner-join-investor">ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ</div>
                    <div className="btn-banner-join">
                        <button>Начать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

}