export default function Banner(){
    return (
        <div className="banner-join-investor">
            <div className="container">
                <div className="wrapper-banner-join-investor">
                    <div className="title-banner-join-investor">Присоединяйся к нашей профессиональной
                        команде
                    </div>
                    <div className="desc-banner-join-investor">ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ</div>
                    <div className="btn-banner-join">
                        <button>Начать</button>
                    </div>
                </div>
            </div>
        </div>

    )
}