export default function Footer() {
    return (
        <div id="footer">
            <div className="container">
                <div className="footer-wrapper">
                    <div className="block-footer">
                        <div className="column-footer-first">
                            <div className="logo">
                                <img src="/image/logo-footer.png" alt="" />
                            </div>
                            <div className="description-footer">В отличии от lorem ipsum, текст рыба на русском языке
                                наполнит
                            </div>
                            <div className="social-icon">
                                <div className="facebook">
                                    <img src="/image/icon/facebook.png" alt="" />
                                </div>
                                <div className="twitter">
                                    <img src="/image/icon/twitter.png" alt="" />
                                </div>
                                <div className="instagram">
                                    <img src="/image/icon/instagram.png" alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="column-footer">
                            <div className="name-column-footer">Инвестору</div>
                            <ul className="submane-ul">
                                <li>Стартапы</li>
                                <li>Инвестпроекты</li>
                                <li>Кредиты Бизнесу</li>
                                <li>Онлайн Кредит</li>
                            </ul>
                        </div>
                        <div className="column-footer">
                            <div className="name-column-footer">Бизнесмену</div>
                            <ul className="submane-ul">
                                <li>Получить Займ</li>
                                <li>Продать Бизнес</li>
                                <li>Программу Запросы</li>
                                <li>Запросы Инвесторов</li>
                            </ul>
                        </div>
                        <div className="column-footer">
                            <div className="name-column-footer">Профессионалу</div>
                            <ul className="submane-ul">
                                <li>Создать Программу</li>
                                <li>Продать Услугу</li>
                            </ul>
                        </div>
                        <div className="column-footer">
                            <div className="name-column-footer">Контакты</div>
                            <div className="adress-footer">
                                Казахстан, Алматы, Пушкина, д.1
                            </div>
                            <div className="tel-footer">
                                <span>+7 (987) 65-43-21</span>
                                <span>+7 (987) 65-43-21</span>
                            </div>
                            <div className="mail-footer">Info@bhub.mytest.kz</div>
                        </div>

                    </div>
                    <div className="underline"></div>
                    <div className="permisson">
                        <p>©2021 Все права защищены</p>
                        <p>Политика конфиденциальности</p>
                    </div>
                </div>
            </div>
        </div>
    )
}