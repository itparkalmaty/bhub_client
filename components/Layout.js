import Header from "./Header";
import Footer from "./Footer";
import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {fetchUser} from "../store/actions/user";

export default function Layout(props) {
    const dispatch = useDispatch()
    useEffect(()=>dispatch(fetchUser()),[])
    return (
        <div>
            <Header/>
            {props.children}
            <Footer/>
        </div>
    )
}

