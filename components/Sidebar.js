import CategoryItem from "./sidebar/Category";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {fetchArticles, searchArticle} from "../store/actions/article";
import ThumbnailItem from "./items/article_thumbnail";
import {searchFreelancers} from "../store/actions/freelancer";
import {fetchByName} from "../store/actions/business";

export default function Sidebar(props){
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(fetchArticles())
    },[])

    const changeHandler = (e)=>{
        switch (props.type) {
            case 'articles':
                dispatch(searchArticle(e.target.value))
            case 'freelancers':
                dispatch(searchFreelancers(e.target.value))
            case 'business':
                dispatch(fetchByName(e.target.value,props.business_type))
            default:
                return false;
        }
    }

    const articles = useSelector(state => state.root.article.articles)
    return (
        <div className="column-right-pos">
            <div className="title-search-column">Поиск</div>
            <div className="form-column-busines">
                <form>
                    <input type="text" placeholder="Введите текст" tabIndex="1" onChange={changeHandler} />
                    <span><img src="/image/icon/search.png" alt="" /></span>
                </form>
            </div>
            <div className="underline-column-busines">
            </div>
            <div className="title-search-item-column">Категории</div>
            <div className="type-category-column">
                {props.categories && props.categories.map(category=><CategoryItem category={category} type={props.type} key={category.id}/>)}
            </div>
            <div className="underline-column-busines">
            </div>
            <div className="title-search-item-column">Популярные новости</div>
            <div className="type-news-column">
                {articles.map(article=><ThumbnailItem article={article} key={article.id}/>)}
            </div>
            <div className="underline-column-busines">
            </div>
            <div className="title-search-item-column">Теги</div>
            <div className="type-tag">
                <div className="tag-item tag-active">Аналитика</div>
                <div className="tag-item">Стартап</div>
                <div className="tag-item">Бизнес</div>
                <div className="tag-item">Стартап</div>
                <div className="tag-item">Бизнес</div>
                <div className="tag-item">Инвестиции</div>
            </div>
            <div className="underline-column-busines">
            </div>
            <div className="title-search-item-column">Мы в соцсетях</div>
            <div className="social-icon">
                <div className="facebook">
                    <img src="/image/icon/facebook.png" alt="" />
                </div>
                <div className="twitter">
                    <img src="/image/icon/twitter.png" alt="" />
                </div>
                <div className="instagram">
                    <img src="/image/icon/instagram.png" alt="" />
                </div>
            </div>
            <div className="underline-column-busines">
            </div>
            <div className="banner-search">
                <img src="/image/pictur/Banner.png" alt="" />
            </div>
        </div>
    )
}