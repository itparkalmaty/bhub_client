import {useDispatch} from "react-redux";
import {fetchByCategory} from "../../store/actions/freelancer";
import {fetchArticleByCategoryId} from "../../store/actions/article";
import {fetchByBusinessCategory} from "../../store/actions/business";
import React from "react";
import ChildCategory from "../items/child_category";
export default function CategoryItem(props) {
    const dispatch = useDispatch()
    const [active,setActive] = React.useState(false)
    return (
        <div>
            <div className={`category-item ${props.category.categories ? 'category-side-bar' : ''} ${active ? 'active' : ''}`} onClick={()=>{
                switch (props.type){
                    case 'articles':
                        return dispatch(fetchArticleByCategoryId(props.category.link))
                    case 'freelancers':
                        return dispatch(fetchByCategory(props.category.link))
                    case 'business':
                        return setActive(!active)
                    default:
                        return 0;
                }
            }}>
                <div className={`name-category `}>{props.category.name}</div>
                <div className="num-category ">{props.category.categories ? props.category.categories.length : props.category.count}</div>

            </div>
            {props.category.categories && active ?
                <div className={'child-categories'}>
                    {props.category.categories.map(category=><ChildCategory category={category}/>)}
                </div> :
                ``
            }
        </div>
    )
}