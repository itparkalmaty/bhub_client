import Link from "next/link";

export default function BusinessTypes(){
    return (
        <div className="button-dropdown types-modal">
            <Link href={'/startups'}>
                <div className="category">
                    Стартап
                </div>
            </Link>
            <Link href={'/businesses'}>
                <div className="category">
                    Бизнесы
                </div>
            </Link>
            <Link href={'/commercial-apartments'}>
                <div className="category">
                    Коммерческие апартаменты
                </div>
            </Link>
        </div>
    )
}