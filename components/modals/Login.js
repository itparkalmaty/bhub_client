import {useState} from "react"
import {useDispatch} from "react-redux";
import {login} from "../../store/actions/user";

export default function Login(props) {
    const [form,setForm] = useState({
        email:'',
        password: '',
    })

    const changeEventHandler = (event) => setForm({...form,[event.target.name]: event.target.value})

    const dispatch = useDispatch()

    return (
        <div className="button-dropdown">
            <div className="title-login">
                <div className="">
                    Добро пожаловать
                </div>
                <i className="fas fa-times" onClick={props.closeLogin}/>
            </div>
            <div className="modal-inputs">

                <div className="">
                    <input type="email" name={'email'} placeholder={'Эл почта'} onChange={changeEventHandler}/>

                </div>
                <div className="">
                    <input type="password" name={'password'} placeholder={'Пароль'} onChange={changeEventHandler}/>

                </div>
            </div>
            <div className="login-buttons">
                <button className={'login_button'} onClick={()=>dispatch(login(form))}>
                    Войти
                </button>
                <button className={'register_button'} onClick={props.openRegister}>
                    Регистрация
                </button>
            </div>
        </div>
    )
}